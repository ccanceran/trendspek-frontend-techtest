# Trendspek Frontend Tech Test

Welcome to Trendspek!

This test will introduce you to a core technology at Trendspek. It will give you exposure to see if you will enjoy working here, and it will give us an understanding of your technical capability.

## Introduction

This is a basic Vue application which incorporates Cesium, which is a library used to display 3D models.

## Installation

* `git clone git@bitbucket.org:haorantrendspek/trendspek-frontend-techtest.git`
* `npm install`
* `npm run serve`

You'll see a message like this:

```
 DONE  Compiled successfully in 7642ms                                    11:05:06 AM

  App running at:
  - Local:   http://localhost:8080/
  - Network: http://10.204.122.84:8080/
```

* Visit the URL provided (http://localhost:8080/ in our example) to see Cesium in action!

----

# Technical Test

## Instructions

Below are three technical challenges. 

1. Create an online repository. (We recommend Bitbucket because you can make it private for free; you can also use Github, Gitlabs, or any other Git-based VCS)
2. Complete all three challenges as best as you are able.
3. Notice for the third challenge, you have three options (3a, 3b or 3c). Please only complete one of these.
4. Update the **Notes** section at the bottom of this document.
5. Commit all changes to your repository.
6. Email a link to this repository to Haoran.

## Criteria

I care about:

* code that works, and caters for edge cases.
* quality of the code, including readability, efficiency, modern-style Javascript, comments and spacing.
* sensible Git commits. I will notice if you commit **everything** as one commit.
* testing. Bonus marks if you can add some form of automated testing, e.g. unit testing, feature testing, or browser testing. (Don't go overboard though, because this is only a test)
* following instructions. Please demonstrate you've read these instructions properly.

## Test 1: CSS

At present, the Cesium tool is hard-coded to 800x600 pixels. Update the CSS so Cesium takes up the full viewport.

Make sure it scales properly because you don't know how big my monitor is :)

## Test 2: Orient Cesium 

Update the code so that the initial view in the browser is facing due North. Make sure you can still see the water tower!

The code for Cesium is in `CesiumViewer.vue`. You can create helper functions as necessary.

References:

* [How do you move the camera?](https://cesium.com/docs/cesiumjs-ref-doc/Viewer.html?classFilter=Viewer#flyTo)
* [How do you locate the centre of the image?](https://cesium.com/docs/cesiumjs-ref-doc/Cesium3DTileset.html?classFilter=3dti#boundingSphere)

----

## Test 3: Cesium Challenge!

Remember, please choose only one of these three tests.

### Test 3a: Axis of Rotation

You will notice that when you rotate the view (by clicking and dragging with the **Middle Mouse Button**), that the point of rotation varies based on the point that you click.

This can be disorienting.

Draw a marker in 3D space that marks the axis of rotation. 

Consult the [Cesium API](https://cesium.com/docs/cesiumjs-ref-doc/) for clues as to how to do this.

### Test 3b: Mark the Road

Draw a line along the short road (just the short section that's part of the 3D model)


Here's an example of what we're after:
![](https://media.discordapp.net/attachments/600646323091406868/666145280474873857/Screen_Shot_2020-01-13_at_4.02.58_pm.png?width=782&height=571)

References: 

* [How do I get the line to stick to the road?](https://sandcastle.cesium.com/?src=Clamp%20to%203D%20Tiles.html)

### Test 3c: FPS Mode

Change the navigation mode in Cesium so that navigation works like a First-Person Shooter. I.e., moving the mouse "looks" around the 3D scene. Pressing WASD moves you forward, left, backwards or right in 3D space.

Bonus points: toggle this mode on and off by hitting Caps Lock.

----

# Applicant's Notes

## Implementation Notes

  Setup
  
      - node version 10 above.
      - clone this repository 'git clone https://ccanceran@bitbucket.org/ccanceran/trendspek-frontend-techtest.git'
      - cd trendspek-frontend-techtest
      - npm install or npm i
      - to start the application run 'npm start'
      - and if you are having a problem accessing this url 'http://localhost:8080/' then try this 'http://127.0.0.1:8080/'

  Run Unit test
  
      - cd trendspek-frontend-techtest
      - run 'jest'

  Run Automation
  
      - cd trendspek-frontend-techtest/automation_test
      - npm install or npm i
      - run 'npm run test:dev' or 'npm run test:dev_default' for headless

## Further Development
Added a button to toggle the view from the modified going back to original to verify the differences.

Also added unit test using Jest and vue/test-utils.
To run the unit test. Make sure that you are in the correct project directory and just run 'jest' in your terminal.

And for the automation you need to make sure that the application is running or else the automation will fail.
Then you need to go to automation_test directory run 'npm install' and then run 'npm run test:dev' and this will open the browser if you want to run the automation in headless just run 'npm run test:dev_default'. Automation is bit slow so you may want to run it in headless.

## Personal Reflection

I found the exam challengig. Censium documentation is not that friendly. I did trial and error for number of time like when I'am doing the rotation of the Camera and the drawing of the line. Also need to focus on the numbers to get the rotation and tilting correctly. The Automation that I'm using in my ReactJs project is similar but the Unit test in Vue is interesting I thought it was totally the same with the one we have in ReactJs, but it is not. Overall I've fun while doing the exam.