@cesium
Feature: Cesium

    Scenario: Test Trendspek Cesium Exam Page
        Given I am on the Home Page
        And I should be seeing that all elements are correct
            | Text Header           | @headerText          |
            | Viewer                | @viewerOrig          |
            | Toggle Button         | @showToggleButton    |
        When I clicked the SHOW ORIGINAL BUTTON
        Then Viwer should switch to original view
        And button label shoud change to SHOW ORIGINAL