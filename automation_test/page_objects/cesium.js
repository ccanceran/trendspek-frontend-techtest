module.exports = {
    elements: {
        headerText: {selector: '//*[@id="title"]', locateStrategy: 'xpath'},
        viewerOrig: {selector: '//*[@id="cesiumContainer"]', locateStrategy: 'xpath'},
        viewerModified: {selector: '//*[@id="cesiumContainerOrig"]', locateStrategy: 'xpath'},
        showToggleButton: {selector: '/html/body/div[1]/div/div[2]', locateStrategy: 'xpath'}
    },
    commands: {
        getXpath: function (elementName) {
            let xpath = '';
            switch (elementName) {
                case ('Text Header'):
                    xpath = '@headerText';
                    break;
                case ('Viewer'):
                    xpath = '@viewerOrig';
                    break;
                case ('Toggle Button'):
                    xpath = '@showToggleButton';
                    break;
            }
            return xpath;
        }
    }
}