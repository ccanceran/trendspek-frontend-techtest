import { client } from 'nightwatch-api';
const { Given, Then, When } = require('cucumber');
const config = require('../util/config');

let cesium = client.page.cesium();

Given(/^I am on the Home Page$/, async () => {
    client.url(config.baseURL);
    await cesium
            .assert.title('trendspek-exam')
            .assert.urlEquals(config.baseURL+'/');
});

Given(/^I should be seeing that all elements are correct$/, async (datatable) => {
    
    client.pause(1000);
    let table = datatable.rawTable;
    let rowSize = table.length;

    console.log(table)
    console.log(rowSize);

    let i = 0;

    do {
        let element = table[i][1];
        console.log(element);
        await cesium.waitForElementVisible(element, 3000, false).assert.visible(element, element + " is visible and available.");
        i++;
    } while (i < rowSize);
});

When(/^I clicked the SHOW ORIGINAL BUTTON$/, async () => {
    await cesium
        .waitForElementVisible('@showToggleButton', 30000, false)
        .assert.visible('@showToggleButton')
        .pause(2000)
        .click('@showToggleButton');
});

Then(/^Viwer should switch to original view$/, async () => {
    await cesium
        .waitForElementVisible('@viewerOrig', 30000, false)
        .assert.visible('@viewerOrig');
});

Then(/^button label shoud change to SHOW ORIGINAL$/, async () => {
    await cesium
        .waitForElementVisible('@showToggleButton', 30000, false)
        .assert.visible('@showToggleButton');
    await cesium.expect.element('@showToggleButton').text.to.contain('SHOW ORIGINAL');
});