import { startWebDriver, stopWebDriver } from 'nightwatch-api';
const testSetting = process.env.TEST_SETTING;
startWebDriver({env: testSetting}).catch(err => console.log(err));

process.once('SIGTERM', async () => {
  try {
    await stopWebDriver();
  } finally {
    process.exit();
  }
});
