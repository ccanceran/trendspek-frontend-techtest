import { setDefaultTimeout, AfterAll, BeforeAll } from 'cucumber';
import { createSession, closeSession } from 'nightwatch-api';

const testSetting = process.env.TEST_SETTING;
setDefaultTimeout(300 * 1000);

BeforeAll(async () => {
  await createSession({ env: testSetting });
});

AfterAll(async () => {
  await closeSession();
});
