const toCamelCase = (text) => {
    return text.split('').reduce((t, v, k) => t + (k === 0 ? v.toLowerCase() : v), '');
};

export default {
    toCamelCase
};