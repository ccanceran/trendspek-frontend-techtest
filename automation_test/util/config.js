const testdata = require('../util/testdata');

module.exports = {
  baseURL: testdata.get_env().domain,
};