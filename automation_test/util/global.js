module.exports = {
    areElementsOnDatatableVisible
}

function areElementsOnDatatableVisible(dataTable, objectList) {
    let table = dataTable.rawTable;
    let rowSize = table.length;
    let element;
    for (let i = 0; i < rowSize; i++) {
        element = objectList.getXpath(table[i][0]);
        objectList.assert.visible(element, element + " is visible and available.");
    }
};