module.exports = {
    //Set the environment that should be accessed. Options: dev, uat, prod
    ENV: process.env.NODE_ENV,

    DEV: {
        domain: "http://127.0.0.1:8080",
    },

    //Functions and Modules
    get_env
};

function get_env() {
    return this.DEV;
}