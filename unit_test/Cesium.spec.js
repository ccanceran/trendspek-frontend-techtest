import { createLocalVue, mount } from '@vue/test-utils'
import Vuex from 'vuex'
import Component from '../src/views/CesiumViewer.vue'

const localVue = createLocalVue()
localVue.use(Vuex)

jest.mock('cesium');

describe('Cesium component', () => {

    let methods = {
        loadCesium: jest.fn(),
        toggleView: jest.fn()
        // more methods...
      }

    test('is a Vue instance', () => {
        let wrapper = mount(Component, { methods });
        expect(wrapper.isVueInstance()).toBeTruthy();
    })

    test('has the header title', () => {
        let wrapper = mount(Component, { methods });
        expect(wrapper.contains('#title')).toBe(true)
    })

    test('has the header title should be Trendspek Exam', () => {
        let wrapper = mount(Component, { methods });
        expect(wrapper.find('#title').text()).toBe('Trendspek Exam')
    })

    test('has the viewer container', () => {
        let wrapper = mount(Component, { methods });
        expect(wrapper.contains('#cesiumContainer')).toBe(true)
    })

    test('has the button switch', () => {
        let wrapper = mount(Component, { methods });
        expect(wrapper.contains('#btn-switch')).toBe(true)
    })

    test('the button switch has the test of Show original', () => {
        let wrapper = mount(Component, { methods });
        expect(wrapper.find('#btn-switch').text()).toBe('Show original')
    })

    test('button is clickable and should trigger toggleView once clicked', () => {
        let wrapper = mount(Component, { methods });
        wrapper.find('#btn-switch').trigger('click');
        expect(methods.toggleView).toHaveBeenCalled();
    })
});
